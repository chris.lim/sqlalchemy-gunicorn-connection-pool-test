import sqlalchemy
from sqlalchemy import Column, Integer, MetaData, String, Table, create_engine, insert
from sqlalchemy_utils import create_database, database_exists

options = {
    "pool_size": 64,
    "max_overflow": 36,
    "pool_recycle": 30,
}
engine = create_engine("mysql+pymysql://root@localhost/test_db", echo=True)

does_db_exist = database_exists(engine.url)
print(f"database_exists: {does_db_exist}")
if not does_db_exist:
    create_database(engine.url)


def create_table(table_name):
    meta = MetaData()

    table = Table(
        table_name,
        meta,
        Column("id", Integer, primary_key=True),
        Column("name", String(100)),
    )
    if not sqlalchemy.inspect(engine).has_table(table_name):
        meta.create_all(engine)
    return table


table_name = "students"
tb = create_table(table_name)

stmt = insert(tb).values(name="spongebob")
compiled = stmt.compile()

with engine.connect() as conn:
    result = conn.execute(stmt)
    print(f"result: {result}")
