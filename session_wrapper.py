from itertools import chain
from typing import Any

from sqlalchemy import event
from sqlalchemy.engine.base import Connection
from sqlalchemy.orm import Query, Session


class SessionWrapper:
    _session: Session
    _flushed_updates: list[Any]

    def __init__(self, bind: Connection) -> None:
        self._session = Session(bind=bind, query_cls=Query, autoflush=False)
        self._flushed_updates = []

        @event.listens_for(self._session, "before_flush")
        def before_flush(session: Session, _flush_context, _instances) -> None:
            self._flushed_updates.extend(
                [repr(update) for update in chain(session.dirty, session.new, session.deleted)]
            )

        @event.listens_for(self._session, "after_commit")
        def after_commit(session: Session) -> None:
            self._flushed_updates.clear()

        @event.listens_for(self._session, "after_rollback")
        def after_rollback(session: Session) -> None:
            self._flushed_updates.clear()

    @property
    def session(self) -> Session:
        return self._session

    def assert_empty_session(self) -> None:
        session_updates = [
            update for update in chain(self._session.dirty, self._session.new, self._session.deleted)
        ] + self._flushed_updates

        if len(session_updates) > 0:
            print("Error: session_updates > 0")
            raise ValueError("session updates > 0")
