# sqlalchemy-gunicorn-connection-pool-test
## Setup
```
pipenv install
pipenv run python sqlalchemy_test.py
```

## Start app and run test
```
# In one terminal startup your app with gunicorn
pipenv run gunicorn chrislimapp:app --config=gunicorn.py --log-level debug

# Send requests in parallel
./send.sh
```