import os

from flask import jsonify, make_response

from app_config import app
from engine import get_engine
from session_manager import SessionManager
from student import Student

session_manager = SessionManager(engine=get_engine())


def get_stats(engine):
    return {
        "pid": os.getpid(),
        "pool_size": engine.pool.size(),
        "connections_checked_in": engine.pool.checkedin(),
        "connections_checked_out": engine.pool.checkedout(),
        "current_overflow": engine.pool.overflow(),
    }


@app.route("/query")
def query():
    with session_manager.get_session() as session:
        result = session.query(Student).update({"name": "spongebob"})
        session.commit()
        stats = get_stats(session_manager._engine)
        result = session.query(Student)
        for student in result:
            print(f"student name: {student.name}")

        return make_response(jsonify(stats), 200)


@app.route("/nestedQuery")
def nestedQuery():
    with session_manager.get_session() as session:
        r1 = session.query(Student)
        nested_stats = {}
        with session_manager.get_session() as session2:
            r2 = session.query(Student)
            r3 = session2.query(Student)
            for student in r1:
                print(f"student name: {student.name}")
            for student in r2:
                print(f"student name: {student.name}")
            for student in r3:
                print(f"student name: {student.name}")
            nested_stats = get_stats(session_manager._engine)
        stats = get_stats(session_manager._engine)

        return make_response(jsonify({"stats": stats, "nested_stats": nested_stats}), 200)


@app.route("/doublyNestedQuery")
def doublyNestedQuery():
    stats = {}
    nested_stats = {}
    doubly_nested_stats = {}
    with session_manager.get_session() as s1:
        with session_manager.get_session() as s2:
            with session_manager.get_session() as s3:
                s1.query(Student)
                s2.query(Student)
                s3.query(Student)
                doubly_nested_stats = get_stats(session_manager._engine)
            nested_stats = get_stats(session_manager._engine)
        stats = get_stats(session_manager._engine)

    return make_response(
        jsonify({"stats": stats, "nested_stats": nested_stats, "doubly_nested_stats": doubly_nested_stats}), 200
    )


@app.route("/")
def status():
    # engine = get_engine()
    # stats = get_stats(engine)
    # return make_response(jsonify(stats), 200)
    stats = get_stats(session_manager._engine)
    return make_response(jsonify(stats), 200)
