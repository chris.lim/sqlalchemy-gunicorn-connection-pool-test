import re
from contextlib import contextmanager
from threading import Lock

from sqlalchemy import event
from sqlalchemy.engine import Engine

from session_wrapper import SessionWrapper

SELECT_PATTERN = re.compile(r"^(SELECT)", flags=re.IGNORECASE)


class SessionManager:
    _lock: Lock
    _engine: Engine

    def __init__(self, engine: Engine) -> None:
        self._lock = Lock()
        self._engine = engine

    @contextmanager
    def get_session(self):
        """
        Context manager yielding a `sqlalchemy.orm.Session` object for database operations.
        """
        # Accepts 'before_cursor_execute' sqlalchemy event params.
        def apply_select_statement_timeout(_conn, _cursor, statement, parameters, _context, _executemany):
            replacement_string = f"\\1 /*+ MAX_EXECUTION_TIME({30_000}) */"
            statement = re.sub(SELECT_PATTERN, replacement_string, statement, count=1)
            return statement, parameters

        connection = self._connect_with_retries()

        session_wrapper = SessionWrapper(bind=connection)

        try:
            event.listen(connection, "before_cursor_execute", apply_select_statement_timeout, retval=True)
            yield session_wrapper.session
            session_wrapper.assert_empty_session()
        except:
            print("exception: session.rollback()")
            session_wrapper.session.rollback()
            raise
        finally:
            try:
                session_wrapper.session.close()
                event.remove(connection, "before_cursor_execute", apply_select_statement_timeout)
            except Exception as e:
                print("finally: error closing session")

            connection.close()

    def _connect_with_retries(self):
        attempts = 0

        while True:
            try:
                with self._lock:
                    connection = self._engine.connect()
                    return connection
            except Exception as e:
                attempts += 1
                if attempts >= 3:
                    raise e
