bind = "127.0.0.1:9999"
workers = 3
threads = 1
loglevel = "info"
timeout = 120


def post_fork(_server, _worker) -> None:  # type: ignore # noqa
    """
    Gunicorn hook that is called just after a worker is forked.
    https://docs.gunicorn.org/en/stable/settings.html#post-fork
    """

    from engine import get_engine

    # Recreate the pool to ensure connections aren't shared across python processes.
    get_engine().pool.recreate()
