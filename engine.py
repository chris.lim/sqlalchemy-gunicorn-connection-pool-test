from sqlalchemy import create_engine

_engine = None


def get_engine():
    global _engine

    print(f"get_engine call engine is None: {_engine is None}")
    if _engine is None:
        options = {
            "echo": True,
            "pool_size": 1,
            "max_overflow": 0,
            "pool_recycle": 30,
        }
        _engine = create_engine("mysql+pymysql://root@localhost/test_db", **options)

    return _engine
